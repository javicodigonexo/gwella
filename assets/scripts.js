jQuery('document').ready(function(){
	clickevents();
	overlayed();
	menu();
	menu_empresa();
});

function overlayed(){
	overl = jQuery(".overlayed").length;
	jQuery(".ovContent").css({
		'top' : '50vh',
		'position' : 'relative',
		'display' : 'block',
		'margin-top' : -(jQuery(".ovContent").outerHeight()/2)+"px"
	});
	if(overl > 0){
		jQuery(".loadbar").animate({'width' : '100%'}, 2000, function(){
			jQuery(".overlayed").animate({'opacity' : 0}, 200, function(){
				jQuery(this).remove();
			})
		})
	}
}

function clickevents(){
	jQuery(".toggleMenu").click(function(){
		jQuery('.wrapAll').toggleClass("show__menu");
	});
}

function menu(){
	$('#accordion').on('show.bs.collapse', function (as) {
		//jQuery(event).addClass("active");
	  // do something…
	elem = as["target"];
	id = $(elem).attr("aria-labelledby");
	$(".panel-heading.active").removeClass("active");
	$("#"+id).addClass("active");
	  
	  console.warn(as["target"])
	  console.warn(as["delegateTarget"])
	  console.warn(as["currentTarget"])
	  
	  for( e in as){
			//console.log(e+" = "+as[e]);
	  }
	});
}

function menu_empresa(){
	jQuery('.menu-empresa li').click(function(){
		li = jQuery(this);
		ref = li.attr('data-ref');
		jQuery('.menu-empresa li').removeClass("in")
		li.addClass("in")
		jQuery('.content-empresa .content').removeClass("in")
		jQuery('#'+ref).addClass("in")
	});
}