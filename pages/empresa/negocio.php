<h1>Negocio y Misión</h1>
<p>
Los servicios de comunicación digital a empresas a través de SMS, eMail, Voz y Fax.

Nuestros servicios se agrupan en dos unidades de negocio.

· Servicios de Marketing Digital
· Servicios de Certificación Digital

El equipo de Marketing digital ofrece servicios personalizados a empresas, para la puesta en marcha de campañas de marketing a través de email, SMS, Voz y Fax. Y el equipo de Certificación digital, centra su actuación en soluciones de comunicación certificada y con validez jurídica.

Nuestros equipos comerciales cuentan con el soporte del departamento técnico, para solucionar y adaptar nuestras herramientas de comunicación a las particularidades de las empresas. Además, nuestro departamento de Atencion al Cliente está preparado y formado para ayudarle en la realización de sus envíos y campañas.
</p>