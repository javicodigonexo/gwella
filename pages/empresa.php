<div class="page--empresa">
	<div class="menu-empresa">
		<ul>
			<li data-ref="rocket" class="in"><img src="<?php echo SITE_URL; ?>/assets/img/rocket.png" class="img-responsive"/></li>
			<li data-ref="globe"><img src="<?php echo SITE_URL; ?>/assets/img/globe.png" class="img-responsive"/></li>
			<li data-ref="flag"><img src="<?php echo SITE_URL; ?>/assets/img/flag.png" class="img-responsive"/></li>
			<li data-ref="settings"><img src="<?php echo SITE_URL; ?>/assets/img/settings.png" class="img-responsive"/></li>
			<li data-ref="check"><img src="<?php echo SITE_URL; ?>/assets/img/check.png" class="img-responsive"/></li>
		</ul>
	</div>
	<div class="content-empresa">
		<div class="content in" id="rocket">1<?php include("empresa/negocio.php"); ?></div>
		<div class="content" id="globe">2<?php include("empresa/negocio.php"); ?></div>
		<div class="content" id="flag">3<?php include("empresa/negocio.php"); ?></div>
		<div class="content" id="settings">4<?php include("empresa/negocio.php"); ?></div>
		<div class="content" id="check">5<?php include("empresa/negocio.php"); ?></div>
	</div>
</div>
<div class="home--footer--logos">
	<img src="<?php echo SITE_URL; ?>/assets/img/footer_logos.jpg" class="img-responsive" />
</div>