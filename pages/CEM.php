<div class="single--page hasFooter">
	<div class="menu clearfix">
		<div class="col-xs-8 left blue"><img src="<?php echo SITE_URL; ?>/assets/img/ddbb.png"/> Customer Engament Marketing</div>
		<div class="col-xs-4 right"><a href="#" class="like"></a><a href="#" class="mail"></a></div>
	</div>
	<div class="content">
		<p>
			Planifica, crea, envía... gestione sus campañas de eMail y SMS Marketing para crear relaciones rentables con tus clientes.
			<br/>
			Una correcta gestión de campañas de Marketing Digital favorece las relaciones duraderas con sus clientes.
			<br/>
			Nuestra plataforma se ha desarrollado para mejorar las comunicaciones con sus clientes, posibilitándole contestar así a las preguntas anteriores.
		</p>
		<table class="general">
			<tr>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/sobre.jpg"/></span>
					<p>EMAILS PERSONALIZADOS</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/reloj.jpg"/></span>
					<p>MENSAJES EN TIEMPO REAL</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/campana.jpg"/></span>
					<p>SMS DE ÚLTIMO MINUTO</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/mobile.jpg"/></span>
					<p>VOICE MESSAGE PARA VENDER</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/org.jpg"/></span>
					<p>SEGMENTACIÓN DE CAMPAÑAS</p></div>
				</td>
			</tr>
			
			<tr>
				<td colspan="5">
					<table class="circles">
						<tr>
							<td><img src="<?php echo SITE_URL; ?>/assets/img/1.jpg"  class="img-responsive"/></td>
							<td><img src="<?php echo SITE_URL; ?>/assets/img/2.jpg"  class="img-responsive"/></td>
							<td><img src="<?php echo SITE_URL; ?>/assets/img/3.jpg"  class="img-responsive"/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php require("footer.php"); ?>
</div>