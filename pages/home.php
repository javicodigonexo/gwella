<div class="home--container">
	<a href="<?php echo SITE_URL; ?>/marketing/">
		<div class="home--element clearfix">
			<div class="col-sm-4 col-xs-6"><img src="<?php echo SITE_URL; ?>/assets/img/MK_Digital.png" class="img-circle img-responsive" /></div>
			<div class="col-sm-8 col-xs-6">
				<h1>Marketing digital</h1>
				<p>Ponemos a su disposición toda la tecnología con los mejores profesionales.</p>
			</div>
		</div>
	</a>
	<a href="<?php echo SITE_URL; ?>/certificacion/">
		<div class="home--element clearfix">
			<div class="col-sm-4 col-xs-6"><img src="<?php echo SITE_URL; ?>/assets/img/CD_Digital.png" class="img-circle img-responsive" /></div>
			<div class="col-sm-8 col-xs-6">
				<h1>Certificación digital</h1>
				<p>Ponemos a su disposición toda la tecnología con los mejores profesionales.</p>
			</div>
		</div>
	</a>
</div>
<div class="home--footer">
	Comunicaciones digitales con valor añadido
</div>
<div class="home--footer--logos">
	<img src="<?php echo SITE_URL; ?>/assets/img/footer_logos.jpg" class="img-responsive" />
</div>