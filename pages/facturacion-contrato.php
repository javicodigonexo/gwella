<div class="single--page hasFooter">
	<div class="menu clearfix">
		<div class="col-xs-8 left green"><img src="<?php echo SITE_URL; ?>/assets/img/ddbb.png"/> Facturación y contrato certificado</div>
		<div class="col-xs-4 right"><a href="#" class="mail"></a></div>
	</div>
	<div class="content">
		<p>
			De manera sencilla y casi igual que enviaría un SMS o un eMail, puede enviar sus contratos y facturas.
			<br/>
En GMS posibilitamos su envío, desde su gestor de eMail o desde nuestra plataforma de envíos.
			<br/>

Además, para facilitar sus comunicaciones electrónicas certificadas ponemos a su disposición diferentes accesos.
		</p>

		<div class="clearfix details">
			<div class="col-xs-6"><div><p>Entorno web desde donde realizar los envíos de SMS o eMail certificados.</p><div class="">PLATAFORMA WEB</div></div></div>
			<div class="col-xs-6"><div><p>Integramos nuestros servicios en su sistema facilitando las herramientas de envío.</p><div class="">API´S</div></div></div>
			<div class="col-xs-6"><div><p>Protocolo estándar abierto que facilita las comunicaciones de mensajes cortos entre los usuarios y el proveedor de mensajería.</p><div class="">SMPP</div></div></div>
			<div class="col-xs-6"><div><p>Nuestro departamento de Atencion al Cliente está a su disposición para facilitarle la gestion de los envíos.</p><div class="">ATC</div></div></div>
			<div class="col-xs-6"><div><p>Desarrollamos un sistema de envíos para que pueda realizar las comunicaciones electrónicas de manera sencilla, eficaz y segura.</p><div class="">DESARROLLO ADHOC</div></div></div>
			</div>
		</div>
	</div>
	<?php require("footer.php"); ?>
</div>