<div class="cd--page hasFooter">
	<div class="cd--header">
	<h2>Servicios de certificación digital</h2>
	</div>
	
	<div class="mk--element">
		<a href="<?php echo SITE_URL; ?>/sms-certificado/">
			<span><img src="<?php echo SITE_URL; ?>/assets/img/cd--sms.png" class="img-responsive"/></span>
			<div>
				<h2>SMS Certificado</h2>
				<p>
					Es un mensaje corto de texto que en la entrega genera un certificado que lo dota de valor jurídico.
				</p>
			</div>
		</a>
	</div>

	<div class="mk--element">
		<a href="<?php echo SITE_URL; ?>/email-certificado/">
			<span><img src="<?php echo SITE_URL; ?>/assets/img/cd--msg.png" class="img-responsive"/></span>
			<div>
				<h2>eMail Certificado</h2>
				<p>
					Se trata de un eMail enviado a través de su gestor de correo y dirigido a una dirección específica de GMS.
				</p>
			</div>
		</a>
	</div>

	<div class="mk--element">
		<a href="<?php echo SITE_URL; ?>/facturacion-contrato/">
			<span><img src="<?php echo SITE_URL; ?>/assets/img/cd--factura.png" class="img-responsive"/></span>
			<div>
				<h2>Facturación y contrato certificado</h2>
				<p>
					Validez jurídica, procesos ágiles, informes, custodia de mensajes, accesibilidad.
				</p>
			</div>
		</a>
	</div>

	<?php require("footer.php"); ?>
</div>