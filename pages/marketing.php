<div class="mk--page hasFooter">
	<div class="mk--header">
	<h2>Servicios de marketing digital</h2>
	</div>
	<div class="mk--element"><a href="<?php echo SITE_URL; ?>/CEM/">
		<span><img src="<?php echo SITE_URL; ?>/assets/img/mk--cog.png" class="img-responsive"/></span>
		<div>
			<h2>Costumer Engagement Marketing</h2>
			<p>
				Gestione sus campañas de eMail y SMS Marketing para crear relaciones duraderas y rentables con sus clientes...
			</p>
		</div></a>
	</div>

	<div class="mk--element"><a href="<?php echo SITE_URL; ?>/sms-mk/">
		<span><img src="<?php echo SITE_URL; ?>/assets/img/mk--mobile.png" class="img-responsive"/></span>
		<div>
			<h2>SMS Marketing</h2>
			<p>
	Mayor tasa de apertura, Click to Action, impacto instantáneo, comunicación One to One: una herramienta imbatible.		</p>
		</div></a>
	</div>

	<div class="mk--element"><a href="<?php echo SITE_URL; ?>/email-mk/">
		<span><img src="<?php echo SITE_URL; ?>/assets/img/mk--email.png" class="img-responsive"/></span>
		<div>
			<h2>eMail Marketing Management</h2>
			<p>
				De manera sencilla y fácil podrá gestionar sus bbdd, manejar las respuestas y organizar los leads.
			</p>
		</div></a>
	</div>

	<div class="mk--element"><a href="<?php echo SITE_URL; ?>/message-mk/">
		<span><img src="<?php echo SITE_URL; ?>/assets/img/mk--msg.png" class="img-responsive"/></span>
		<div>
			<h2>Voice Message Marketing</h2>
			<p>
				Miles de mensajes de voz de manera simultánea. Más convincentes, más efectivos.
			</p>
		</div></a>
	</div>
	<?php require("footer.php"); ?>
</div>