<div class="single--page hasFooter">
	<div class="menu clearfix">
		<div class="col-xs-8 left blue"><img src="<?php echo SITE_URL; ?>/assets/img/ddbb.png"/> eMail Marketing</div>
		<div class="col-xs-4 right"><a href="#" class="like"></a><a href="#" class="mail"></a></div>
	</div>
	<div class="content">
		<p>
			De manera sencilla y fácil podrá gestionar sus bbdd, manejar las respuestas y organizar los leads.
			<BR/>
			De manera sencilla y fácil podrá gestionar sus bbdd, manejar las respuestas, organizar los leads, planificar las fechas de campaña, analizar los resultados...
			<BR/>
			Mejore el ROI de sus campañas con su nueva plataforma profesional para el eMail Marketing.
		</p>
		<table class="general">
			<tr>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/db.jpg"/></span>
					<p>GESTIÓN BBDD</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/sobre.jpg"/></span>
					<p>CREACIÓN DE EMAILS</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/target.jpg"/></span>
					<p>GESTION DE LEADS</p></div>
				</td>
				<td class="arrow"><div>
					<span><img src="<?php echo SITE_URL; ?>/assets/img/chart2.jpg"/></span>
					<p>ANÁLISIS DE CAMPAÑAS</p></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<table class="circles">
						<tr>
							<td><img src="<?php echo SITE_URL; ?>/assets/img/7.jpg"  class="img-responsive"/></td>
							<td><img src="<?php echo SITE_URL; ?>/assets/img/8.jpg"  class="img-responsive"/></td>
							<td><img src="<?php echo SITE_URL; ?>/assets/img/9.jpg"  class="img-responsive"/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php require("footer.php"); ?>
</div>