﻿<?php $collapse = 0; ?>
<div class="menuContent">

	<img src="<?php echo SITE_URL; ?>/assets/img/logo-white.png" class="menu-logo"/>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  
	  
	    <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading active" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed"  href="<?php echo SITE_URL; ?>/empresa/" aria-expanded="false" aria-controls="collapse-<?php echo $collapse; ?>">
			  <img src="<?php echo SITE_URL; ?>/assets/img/minilogo.png"/>Empresa
			</a>
		  </h4>
		</div>
		<div id="collapse-<?php echo $collapse; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-<?php echo $collapse; ?>">
		  <div class="panel-body">
			<ul>
				<li><a href="<?php echo SITE_URL; ?>/GMS/">GMS</a></li>
				<li><a href="<?php echo SITE_URL; ?>/negocio-y-vision/">Negocio y Visión</a></li>
				<li><a href="<?php echo SITE_URL; ?>/trayectoria/">Trayectoria</a></li>
				<li><a href="<?php echo SITE_URL; ?>/tecnologia/">Tecnología</a></li>
				<li><a href="<?php echo SITE_URL; ?>/certificados/">Certificados</a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  
	  <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button"  href="<?php echo SITE_URL; ?>/marketing/" aria-expanded="false" >
				<img src="<?php echo SITE_URL; ?>/assets/img/mobile.png"/> Servicio de Marketing Digital
			</a>
		  </h4>
		</div>
	  </div>
	  
	  
	  <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button"  href="<?php echo SITE_URL; ?>/certificacion/" aria-expanded="false" >
			 <img src="<?php echo SITE_URL; ?>/assets/img/certificate.png"/> Servicio de Certificados Digitales
			</a>
		  </h4>
		</div>
	  </div>
	  
	  <?php /*
	   <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading active" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $collapse; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $collapse; ?>">
			  Empresa
			</a>
		  </h4>
		</div>
		<div id="collapse-<?php echo $collapse; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-<?php echo $collapse; ?>">
		  <div class="panel-body">
			<ul>
				<li><a href="<?php echo SITE_URL; ?>/GMS/">GMS</a></li>
				<li><a href="<?php echo SITE_URL; ?>/negocio-y-vision/">Negocio y Visión</a></li>
				<li><a href="<?php echo SITE_URL; ?>/trayectoria/">Trayectoria</a></li>
				<li><a href="<?php echo SITE_URL; ?>/tecnologia/">Tecnología</a></li>
				<li><a href="<?php echo SITE_URL; ?>/certificados/">Certificados</a></li>
			</ul>
		  </div>
		</div>
	  </div>
	  
	  <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $collapse; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $collapse; ?>">
			  Servicio de Marketing Digital
			</a>
		  </h4>
		</div>
		<div id="collapse-<?php echo $collapse; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $collapse; ?>">
		  <div class="panel-body">
				<ul>
					<li><a href="<?php echo SITE_URL; ?>/CEM/">Customer Engament Marketing</a></li>
					<li><a href="<?php echo SITE_URL; ?>/sms-mk/">SMS Marketing</a></li>
					<li><a href="<?php echo SITE_URL; ?>/email-mk/">eMail Marketing</a></li>
					<li><a href="<?php echo SITE_URL; ?>/message-mk/">Voice Message Marketing</a></li>
				</ul>
			</div>
		</div>
	  </div>
	  
	  
	  
	  
	   <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $collapse; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $collapse; ?>">
			  Servicio de Certificados Digitales
			</a>
		  </h4>
		</div>
		<div id="collapse-<?php echo $collapse; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $collapse; ?>">
		  <div class="panel-body">
			<ul>
				<li><a href="<?php echo SITE_URL; ?>/sms-certificado/">SMS Certificado</a></li>
				<li><a href="<?php echo SITE_URL; ?>/email-certificado/">eMail Certificado</a></li>
				<li><a href="<?php echo SITE_URL; ?>/facturacion-contrato-certificado/">Facturación y contrato certificado</a></li>
				</ul>
			</div>
		</div>
	  </div>
	  */
	  ?>
	  
	  <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button"  href="<?php echo SITE_URL; ?>/newsletter/" aria-expanded="false" >
			  <img src="<?php echo SITE_URL; ?>/assets/img/menumail.png"/> Suscripción a Newsletter
			</a>
		  </h4>
		</div>
	  </div>
	  
	  <?php $collapse++; ?>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button"  href="<?php echo SITE_URL; ?>/contacto/" aria-expanded="false" >
			  <img src="<?php echo SITE_URL; ?>/assets/img/phone.png"/>Contacto
			</a>
		  </h4>
		</div>
	  </div>
	  
	  <?php $collapse++; ?>
	  <div class="panel panel-default ajustes">
		<div class="panel-heading" role="tab" id="heading-<?php echo $collapse; ?>">
		  <h4 class="panel-title">
			<a class="collapsed" role="button"  href="<?php echo SITE_URL; ?>/ajustes/" aria-expanded="false" >
			  <img src="<?php echo SITE_URL; ?>/assets/img/cog.png"/> Ajustes
			</a>
		  </h4>
		</div>
	  </div>
	  
	</div>
</div>