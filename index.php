<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"    "http://www.w3.org/TR/html4/strict.dtd">
<?php
	
	define("SITE_URL", "http://localhost/gwella"); 

	include "assets/lessCompiler/lessc.inc.php";
	$less = new lessc;
	try {
		$less->checkedCompile("assets/style.less", "assets/style.css");
	} catch (Exception $ex) {
		echo "lessphp fatal error: assets/".$ex->getMessage();
	}
?>
<HTML>
	<HEAD>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link href="<?php echo SITE_URL; ?>/assets/style.css" rel="stylesheet" type="text/css">
		<TITLE>GWELLA</TITLE>
	</HEAD>
	<BODY>
		<?php  if(!isset($_GET["p"])): ?>
		<div class="container">
			<div class="overlayed container">
				<div class="ovContent">
					<img src="<?php echo SITE_URL; ?>/assets/img/logo-w.png" class="img-responsive logo" />
					<div class="loadbg"><div class="loadbar"></div></div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="container containerAll">
			<div class="wrapAll">
				<div class="wrapMenu">
					<?php require("menu.php");?>
				</div>
				<div class="wrapContent">
				<header><?php require("header.php");?></header>
				<?php
					if(isset($_GET["p"]) && file_exists("pages/".$_GET["p"].".php")): 
						require("pages/".$_GET["p"].".php");
					elseif(isset($_GET["p"])  && !file_exists("pages/".$_GET["p"].".php")):
						require("pages/404.php");
					else:
						require("pages/home.php");
					endif;
				?>
				</div>
			</div>
		</div>
		
	</BODY>
</HTML>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="<?php echo SITE_URL; ?>/assets/scripts.js"></script>